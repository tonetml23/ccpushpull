if [ "$#" -eq 1 ]; then
    echo "Buscar objetos cuando no hay:"
    curl ${1}/buscarTodos -d "$(cat ej_buscar_todos.json)"
    echo ""
    echo ""

    echo "Insertar un objeto:"
    curl ${1}/insertar -d "$(cat ej_insertar.json)"
    echo ""
    echo ""
    
    echo "Insertar un objeto cuya clave ya existe (Debe dar ERROR):"
    curl ${1}/insertar -d "$(cat ej_insertar.json)"
    echo ""
    echo ""
    
    echo "Insertar un objeto distinto:"
    curl ${1}/insertar -d "$(cat ej_insertar2.json)"
    echo ""
    echo ""

    echo "Buscar el primer objeto insertado por su clave:"
    curl ${1}/buscarPorClave -d "$(cat ej_buscar_clave.json)"
    echo ""
    echo ""
    
    echo "Buscar todos los objetos:"
    curl ${1}/buscarTodos -d "$(cat ej_buscar_todos.json)"
    echo ""
    echo ""
    
    echo "Buscar el segundo objeto mediante valor (Se busca la cadena \"tortilla\"):"
    curl ${1}/buscarPorValor -d "$(cat ej_buscar_valor.json)"
    echo ""
    echo ""
    
    echo "Buscar una clave que no existe (debe dar ERROR):"
    curl ${1}/buscarPorClave -d "$(cat ej_buscar_clave_no_existe.json)"
    echo ""
    echo ""
    
    echo "Buscar un valor que no existe (debe dar ERROR):"
    curl ${1}/buscarPorValor -d "$(cat ej_buscar_valor_no_existe.json)"
    echo ""
    echo ""
    
else
    echo "No se ha indicado la url de la api a la que enviar preticiones."
    echo "USAGE: $0 url"
fi
