//docker run --name natssrv --rm  -p 5555:4222 nats -js
const LogicBD = require("../include/logicBD/LogicBD.js")
const Cola = require("../include/natsJetstream/cola.js")

const SERVIDOR_NATS_URL =   "localhost:5555" 
const URL_REDIS = "redis://127.0.0.1:6379"
const urlRedis = (process.argv[3] ?
    process.argv[3] :
    URL_REDIS)

// //Clase que contiene todo lo de NATS
// class Cola {
//     constructor(nUrl, streamData) {
//         this.nUrl = nUrl
//         this.streamData = streamData
//     }

//     async conectarConNats() {
//         this.nc = await connect( this.nUrl )
//         this.jsManager = await this.nc.jetstreamManager()
//         this.jsClient = await this.nc.jetstream()
//         this.jsManager.streams.add( this.streamData )
//     }
// }

//Funcion para parar la ejecucion
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function connectAndSubscribe() {
    //Conexión a nats y jetstream
    cola = new Cola(SERVIDOR_NATS_URL)
    await cola.conectarConNats();
    const workerName = "worker";

    // Cargar logica
    const logicBD = new LogicBD(urlRedis)
    console.log(urlRedis)

    //Abrir conexion con Redis
    logicBD.openConnection()

    try {
        // Subscripción a la cola
        suscripcionCliente = await cola.jsClient.pullSubscribe(
            cola.tema_insertar, 
            { config: { durable_name: workerName } }
        )
    } catch( error ) {
        throw "Error al realizar subscripcion del frontend: " + error 
    }

    //Si se recibe SIGINT salir
    process.on('SIGINT', async function() {
        console.log (" sigint capturada ! ")
        // cerrar conexion con nats
        await cola.nc.close()

        const err = await cola.nc.closed()
        if ( ! err ) {
            console.log( "desconexion ok" )
        } else {
            console.log( err )
        }
        process.exit(0)
    })

    //Pedir trabajos en bucle
    while(true) {
        try {
			let m = await cola.jsClient.pull( cola.datosStream.name, 
										  workerName )
            if (m != null) {
                console.log("Mensaje: " + cola.sc.decode(m.data))

                //Parsear JSON y obtener datos
                datos = JSON.parse(m.data)
                const owner = datos["usuario"]
                const key = datos["clave"]
                const object = datos["valor"]

                //Insertar objeto JSON en la BD
                var response = await logicBD[ "insertObject" ]( owner, key, object )
                mensaje = "ERROR: Ha ocurrido un error y no se ha podido insertar el objeto."
                if (response) {
                    if (response.value == false) {
                        mensaje = "ERROR: No se ha insertado el objeto. " + response.msg
                    }
                    else {
                        mensaje = "Se ha insertado el objeto correctamente"
                    }
                }
                //Responder por el tema de respuesta
                cola.nc.publish(cola.tema_respuesta+"-"+datos["idTrabajo"],
                    cola.sc.encode(mensaje))
                await m.ack()
                console.log(mensaje)
            }
		} catch( error ) {
            if (String(error) != "NatsError: no messages")
                console.log("Error en el bucle de trabajos: " + error)
		}
        await sleep(400)
    }
}

connectAndSubscribe();