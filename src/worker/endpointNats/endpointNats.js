// ---------------------------------------------------------------------
// main.js
// ---------------------------------------------------------------------
const LogicBD = require("../logicBD/LogicBD.js")

const URL_REDIS = "redis://127.0.0.1:6379"
const urlRedis = (process.argv[2] ?
    process.argv[2] :
    URL_REDIS)

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

async function main() {    

    // Cargar logica
    const logicBD = new LogicBD(urlRedis)
    console.log(urlRedis)

    //Abrir conexion con Redis
    logicBD.openConnection()
   
    
    //JSON de prueba en formato texto
    const owner = 'anton'
    const key = 'clave2'
    const object = 
    {			
        "dia": "jueves",
        "tarea": "desayunar",
        "objetos": ["cafetera", "tostada", "leche", "cafe"]
    }    
    //Insertar objeto JSON en la BD
	var response = await logicBD[ "insertObject" ]( owner, key, object )
    if (response) {
        //Mostrar respuesta
        console.log('Insert JSON: ' +response.value);
        console.log(response.msg);
    }

    //Buscar objecto por clave y propietario
	var response = await logicBD[ "getObject" ]( owner, key )
    if (response) {
        //Mostrar respuesta
        console.log('Get JSON: ' + JSON.stringify(response.value));
        console.log(response.msg);
    }

    //Buscar todos los objetos de un propietario
    //Devuelve un array de JSON
	var response = await logicBD[ "getObjects" ]( owner )
    if (response) {
        //Mostrar respuesta
        console.log('Get all JSON: ' + JSON.stringify(response.value));
        console.log(response.msg);
    }

    // Capturar ctrl-c para cerrar el programa	
	process.on('SIGINT', async function() {
        logicBD.closeConnection()
	})

    //Buscar objetos que contengan un valor determinado
    const value = 'm'
    var response = await logicBD[ "getObjects" ]( owner )
    if (response) {
        //Mostrar respuesta
        
        console.log('Get json by condition: ' + JSON.stringify(response.value.filter(object => JSON.stringify(object).includes(value))));
        console.log(response.msg);
    }
   
} // End main

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
main()