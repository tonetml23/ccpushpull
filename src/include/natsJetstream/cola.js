const { connect, StringCodec, Subscription,
		AckPolicy, WorkQueuePolicy, consumerOpts, createInbox }
	  = require( "nats" )

const TEMA_RESPUESTA = "respuesta"
const TEMA_TRABAJOS = "trabajos"
const TEMA_INSERTAR = "insertar"

//Clase que contiene todo lo de NATS
module.exports =
class Cola {
    constructor(nUrl) {
        const urlNats = 
        {
            servers: ( process.argv[2] ?  process.argv[2] : nUrl )
        }
        this.nUrl = urlNats
        this.datosStream = {
            name: TEMA_TRABAJOS,
            subjects: [ TEMA_INSERTAR ], 
            retention: WorkQueuePolicy,
        } 
        this.tema_respuesta = TEMA_RESPUESTA
        this.tema_trabajos = TEMA_TRABAJOS
        this.tema_insertar = TEMA_INSERTAR
        this.sc = StringCodec();
    }

    async conectarConNats() {
        this.nc = await connect( this.nUrl )
        this.jsManager = await this.nc.jetstreamManager()
        this.jsClient = await this.nc.jetstream()
        this.jsManager.streams.add( this.datosStream )
    }
}