const { Console } = require("console");
const http = require("http");
const host = 'localhost';
const port = 4545;

const SERVIDOR_NATS_URL =   "localhost:5555" 
const { mainModule } = require("process");
const TIEMPO_TIMEOUT = 2000
const NOMBRE_FRONTEND = "frontend"

const URL_REDIS = "redis://127.0.0.1:6379"
const urlRedis = (process.argv[3] ?
    process.argv[3] :
    URL_REDIS)

const Cola = require("../include/natsJetstream/cola.js")
const LogicBD = require("../include/logicBD/LogicBD.js")

//Obtiene un string aleatorio
function getRandomString() {
	let fecha = ( Date.now() % 123456789 ).toString()
	let sufijo =  Math.random().toString(36).substr(2,8) 
	return fecha+sufijo
}

async function procesarPeticion(req, res, body, cola, logicBD) {
    datos = {}
    //Parsear JSON devolver error si no se puede
    try {
        datos = JSON.parse(body);
    }
    catch(error) {
        res.writeHead(400, { "Content-Type": "text/html" })
        res.end("Error: peticion invalida. No es un objeto JSON")
        return
    }

    //Peticion insertar
    if (req.method == 'POST' && req.url == "/insertar") {
        if (!datos || !datos.hasOwnProperty("usuario") 
            || !datos.hasOwnProperty("clave")
            || !datos.hasOwnProperty("valor")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        datos["idTrabajo"] = getRandomString();

        //Subscribirse al tema de respuesta
        subRespuesta = cola.nc.subscribe(
            cola.tema_respuesta+"-"+datos["idTrabajo"],
            { 
                max: 1, 
                callback: function( err, resp ) {
                    mensaje = resp;
                    if ( err ) {
                        mensaje = "Ha habido un error."
                        return
                    }

                    //Responder el resultado recibido
                    res.writeHead(200, { "Content-Type": "text/html" });
                    res.end(String(resp.data));
                }
            }
        )

        //Timeout
        setTimeout( async () => {
            try { 
                await subRespuesta.close()
                res.writeHead(200, { "Content-Type": "text/html" });
                res.end("TIMEOUT: Se ha vencido el tiempo de espera para obtener una respuesta.");
            } catch( err ) {
                res.writeHead(200, { "Content-Type": "text/html" });
                res.end("TIMEOUT: Se ha vencido el tiempo de espera para obtener una respuesta.\n" + err);
            }
        }, TIEMPO_TIMEOUT )
        
        console.log("Recibido: " + body);

        //Publicar mensaje en jetstream
        await cola.jsClient.publish(
            cola.tema_insertar, 
            cola.sc.encode(JSON.stringify(datos)),
            { //Incluir id del mensaje evita repetidos
                msgID: cola.tema_insertar + "-" + datos["idTrabajo"]
            }
        );
    }
    //Peticion buscar por clave
    else if (req.method == 'POST' && req.url == "/buscarPorClave") {
        if (!datos || !datos.hasOwnProperty("usuario")
            || !datos.hasOwnProperty("valor-busqueda")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        owner = datos["usuario"]
        key = datos["valor-busqueda"]
        var response = await logicBD[ "getObject" ]( owner, key )
        if (response) {
            //Mostrar respuesta
            data = JSON.stringify(response.value);
            if (data) {
                console.log('Get JSON: ' + data);
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end(data)
            }
            else {
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end("No se han encontrado datos")
            }
        }
        else {
            res.writeHead(200, { "Content-Type": "text/html" })
            res.end("No se ha encontrado el objeto buscado")
        }
    }
    //Peticion buscar todos
    else if (req.method == 'POST' && req.url == "/buscarTodos") {
        if (!datos || !datos.hasOwnProperty("usuario")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        owner = datos["usuario"]
        var response = await logicBD[ "getObjects" ]( owner )
        if (response) {
            //Mostrar respuesta
            data = JSON.stringify(response.value);
            if (data) {
                console.log('Get JSON: ' + data);
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end(data)
            }
            else {
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end("No se han encontrado datos")
            }
        }
        else {
            res.writeHead(200, { "Content-Type": "text/html" })
            res.end("No se han encontrado datos")
        }
    }
    //Peticion buscar por valor
    else if (req.method == 'POST' && req.url == "/buscarPorValor") {
        if (!datos || !datos.hasOwnProperty("usuario")
            || !datos.hasOwnProperty("valor-busqueda")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        owner = datos["usuario"]
        value = datos["valor-busqueda"]
        var response = await logicBD[ "getObjects" ]( owner )
        if (response) {
            //Mostrar respuesta
            data = JSON.stringify(response.value.filter(object => JSON.stringify(object).includes(value)))
            if (data) {
                console.log('Get JSON: ' + data);
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end(data)
            }
            else {
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end("No se han encontrado datos")
            }
        }
        else {
            res.writeHead(200, { "Content-Type": "text/html" })
            res.end("No se han encontrado datos")
        }
    }
    else {
        res.writeHead(200, { "Content-Type": "text/html" })
        res.end("No se reconoce la peticion " + req.url)
    }
}

async function main() {
    //Conexión y configuración de nats
    cola = new Cola(SERVIDOR_NATS_URL)
    await cola.conectarConNats();

    // Cargar logica
    const logicBD = new LogicBD(urlRedis)
    console.log(urlRedis)

    //Abrir conexion con Redis
    await logicBD.openConnection()

    const requestListener = function (req, res) {
        var body = "";
        //Imagino que esto es para capturar todos los paquetes
        req.on("data", function (chunk) {
            body += chunk;
        });

        //Procesar peticion
        req.on("end", async function(){
            procesarPeticion(req, res, body, cola, logicBD)
        })
    }

    const server = http.createServer(requestListener);
    server.listen(port, host, () => {
        console.log(`Server is running on http://${host}:${port}`);
    });
}

main()
