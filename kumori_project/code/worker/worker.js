//docker run --name natssrv --rm  -p 5555:4222 nats -js
const LogicBD = require("./include/logicBD/LogicBD.js")
const Cola = require("./include/natsJetstream/cola.js")
const fs = require('fs')

const configFile = process.env.CONFIG_FILE

// const SERVIDOR_NATS_URL =   "localhost:5555" 
// const URL_REDIS = "redis://127.0.0.1:6379"

//Funcion para parar la ejecucion
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

readConfig = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(configFile, "utf8", (err, jsonString) => {
      if (err) {
        reject(err)
        return
      }
      try {
        appConfig = JSON.parse(jsonString)
        console.log(jsonString)
        resolve(appConfig)
      } catch (err) {
        reject(err)
      }
    })
  })
}

async function connectAndSubscribe(url_nats, url_redis) {
    //Conexión a nats y jetstream
    cola = new Cola(url_nats)
    await cola.conectarConNats();
    const workerName = "worker";

//     Cargar logica
    const logicBD = new LogicBD(url_redis)
    console.log(url_redis)
// 
//     Abrir conexion con Redis
    logicBD.openConnection()
    
    
    subscrito = false
    while(!subscrito) {
        try {
            // Subscripción a la cola
            suscripcionCliente = await cola.jsClient.pullSubscribe(
                cola.tema_insertar, 
                { config: { durable_name: workerName } }
            )
            subscrito = true
        } catch( error ) {
            console.log("No se ha podido conectar. Reintentando... (" + error + ")")
        }
    }

    //Si se recibe SIGINT salir
    process.on('SIGINT', async function() {
        console.log (" sigint capturada ! ")
        // cerrar conexion con nats
        await cola.nc.close()

        const err = await cola.nc.closed()
        if ( ! err ) {
            console.log( "desconexion ok" )
        } else {
            console.log( err )
        }
        process.exit(0)
    })

    //Pedir trabajos en bucle
    while(true) {
        try {
			let m = await cola.jsClient.pull( cola.datosStream.name, 
										  workerName )
            if (m != null) {
                console.log("Mensaje: " + cola.sc.decode(m.data))

                //Parsear JSON y obtener datos
                datos = JSON.parse(m.data)
                const owner = datos["usuario"]
                const key = datos["clave"]
                const object = datos["valor"]

                //Insertar objeto JSON en la BD
                var response = await logicBD[ "insertObject" ]( owner, key, object )
                mensaje = "ERROR: Ha ocurrido un error y no se ha podido insertar el objeto."
                if (response) {
                    if (response.value == false) {
                        mensaje = "ERROR: No se ha insertado el objeto ¿Ya existe un objeto con esa clave?. " + response.msg
                    }
                    else {
                        mensaje = "Se ha insertado el objeto correctamente."
                    }
                }
                //Responder por el tema de respuesta
                cola.nc.publish(cola.tema_respuesta+"-"+datos["idTrabajo"],
                    cola.sc.encode(mensaje))
                await m.ack()
                console.log(mensaje)
            }
		} catch( error ) {
            if (String(error) != "NatsError: no messages")
                console.log("Error en el bucle de trabajos: " + error)
		}
        await sleep(400)
    }
}

config = readConfig()
.then((config) => {
    connectAndSubscribe(config.urlnats, config.urlredis)
})
.catch((err) => {
  console.log(`Error initializating component: ${err.message}`)
})
