const { Console } = require("console");
const http = require("http");
const fs = require('fs')
const host = '0.0.0.0'
const port = process.env.HTTP_SERVER_PORT_ENV
const configFile = process.env.CONFIG_FILE

// const SERVIDOR_NATS_URL =   "localhost:5555" 
const { mainModule } = require("process");
const TIEMPO_TIMEOUT = 3000
const NOMBRE_FRONTEND = "frontend"

// const URL_REDIS = "redis://127.0.0.1:6379"

const Cola = require("./include/natsJetstream/cola.js")
const LogicBD = require("./include/logicBD/LogicBD.js")

//Obtiene un string aleatorio
function getRandomString() {
	let fecha = ( Date.now() % 123456789 ).toString()
	let sufijo =  Math.random().toString(36).substr(2,8) 
	return fecha+sufijo
}

async function procesarPeticion(req, res, body, cola, logicBD) {
    var responded = false
    datos = {}
    //Parsear JSON devolver error si no se puede
    try {
        datos = JSON.parse(body);
    }
    catch(error) {
        res.writeHead(400, { "Content-Type": "text/html" })
        res.end("Error: peticion invalida. No es un objeto JSON")
        return
    }

    //Peticion insertar
    if (req.method == 'POST' && req.url == "/insertar") {
        if (!datos || !datos.hasOwnProperty("usuario") 
            || !datos.hasOwnProperty("clave")
            || !datos.hasOwnProperty("valor")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        datos["idTrabajo"] = getRandomString();

        //Subscribirse al tema de respuesta
        subRespuesta = cola.nc.subscribe(
            cola.tema_respuesta+"-"+datos["idTrabajo"],
            { 
                max: 1, 
                callback: function( err, resp ) {
                    mensaje = resp;
                    if ( err ) {
                        mensaje = "Ha habido un error."
                        return
                    }

                    //Responder el resultado recibido
                    if (!res.finished) {
                        res.writeHead(200, { "Content-Type": "text/html" });
                        res.end(String(resp.data));
                    }
                }
            }
        )

        //Timeout
        setTimeout( async () => {
            try { 
                await subRespuesta.close()
                if (!res.finished) {
                    res.writeHead(200, { "Content-Type": "text/html" });
                    res.end("No hay workers disponibles en este momento. Se proesará la petición cuando sea posible.")
                }
            } catch( err ) {
                if (!res.finished) {
                    res.writeHead(200, { "Content-Type": "text/html" });
                    res.end("TIMEOUT: Se ha vencido el tiempo de espera para obtener una respuesta.\nERROR: " + err);
                }
            }
        }, TIEMPO_TIMEOUT )
        
        console.log("Recibido: " + body);

        //Publicar mensaje en jetstream
        await cola.jsClient.publish(
            cola.tema_insertar, 
            cola.sc.encode(JSON.stringify(datos)),
            { //Incluir id del mensaje evita repetidos
                msgID: cola.tema_insertar + "-" + datos["idTrabajo"]
            }
        );
    }
    //Peticion buscar por clave
    else if (req.method == 'POST' && req.url == "/buscarPorClave") {
        if (!datos || !datos.hasOwnProperty("usuario")
            || !datos.hasOwnProperty("valor-busqueda")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        owner = datos["usuario"]
        key = datos["valor-busqueda"]
        var response = await logicBD[ "getObject" ]( owner, key )
        if (response && response.value) {
            //Mostrar respuesta
            data = JSON.stringify(response.value);
            if (data) {
                console.log('Get JSON: ' + data);
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end(data)
            }
            else {
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end("No se han encontrado datos")
            }
        }
        else {
            res.writeHead(200, { "Content-Type": "text/html" })
            res.end("No se ha encontrado el objeto buscado")
        }
    }
    //Peticion buscar todos
    else if (req.method == 'POST' && req.url == "/buscarTodos") {
        if (!datos || !datos.hasOwnProperty("usuario")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        owner = datos["usuario"]
        var response = await logicBD[ "getObjects" ]( owner )
        if (response && response.value) {
            //Mostrar respuesta
            data = JSON.stringify(response.value);
            if (data) {
                console.log('Get JSON: ' + data);
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end(data)
            }
            else {
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end("No se han encontrado datos")
            }
        }
        else {
            res.writeHead(200, { "Content-Type": "text/html" })
            res.end("No se han encontrado datos")
        }
    }
    //Peticion buscar por valor
    else if (req.method == 'POST' && req.url == "/buscarPorValor") {
        if (!datos || !datos.hasOwnProperty("usuario")
            || !datos.hasOwnProperty("valor-busqueda")) {
            res.writeHead(400, { "Content-Type": "text/html" })
            res.end("Error: peticion invalida. Faltan datos")
            return
        }
        owner = datos["usuario"]
        value = datos["valor-busqueda"]
        var response = await logicBD[ "getObjects" ]( owner )
        if (response && response.value) {
            //Mostrar respuesta
            data = JSON.stringify(response.value.filter(object => JSON.stringify(object).includes(value)))
            if (data && data != "[]") {
                console.log('Get JSON: ' + data);
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end(data)
            }
            else {
                res.writeHead(200, { "Content-Type": "text/html" })
                res.end("No se han encontrado datos")
            }
        }
        else {
            res.writeHead(200, { "Content-Type": "text/html" })
            res.end("No se han encontrado datos")
        }
    }
    else {
        res.writeHead(200, { "Content-Type": "text/html" })
        res.end("No se reconoce la peticion " + req.url)
    }
}

readConfig = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(configFile, "utf8", (err, jsonString) => {
      if (err) {
        reject(err)
        return
      }
      try {
        appConfig = JSON.parse(jsonString)
        console.log(jsonString)
        console.log(appConfig)
        console.log(appConfig.urlnats)
        console.log(appConfig.urlredis)
        resolve(appConfig)
      } catch (err) {
        reject(err)
      }
    })
  })
}

async function main(nats_endpoint, redis_endpoint) {
    //Conexión y configuración de nats
    cola = new Cola(nats_endpoint)
    await cola.conectarConNats();

    //Cargar logica
    const logicBD = new LogicBD(redis_endpoint)
    console.log(redis_endpoint)

    //Abrir conexion con Redis
    await logicBD.openConnection()
    
    //Si se recibe SIGINT salir
    process.on('SIGINT', async function() {
        console.log (" sigint capturada ! ")
        // cerrar conexion con nats
        await cola.nc.close()

        const err = await cola.nc.closed()
        if ( ! err ) {
            console.log( "desconexion ok" )
        } else {
            console.log( err )
        }
        process.exit(0)
    })

    const requestListener = function (req, res) {
        var body = "";
        //Imagino que esto es para capturar todos los paquetes
        req.on("data", function (chunk) {
            body += chunk;
        });

        //Procesar peticion
        req.on("end", async function(){
            try {
                procesarPeticion(req, res, body, cola, logicBD)
            }
            catch(error) {
                console.log("Ha ocurrido un error inesperado: " + error)
            }
        })
    }

    const server = http.createServer(requestListener);
    server.listen(port, host, () => {
        console.log(`Server is running on http://${host}:${port}`);
    });
}

config = readConfig()
.then((config) => {
    console.log("NATS"+config.urlnats)
    console.log("REDIS"+config.urlredis)
    main(config.urlnats, config.urlredis)
})
.catch((err) => {
    console.log(`Error initializating component: ${err.message}`)
})
