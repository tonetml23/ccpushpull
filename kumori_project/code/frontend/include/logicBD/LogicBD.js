// ---------------------------------------------------------------------
// LogicaBD.js
// ---------------------------------------------------------------------

const redis = require("redis");

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
module.exports =
	class LogicaBD {
		constructor(urlRedis) {
			//Almaceno la url para la conexión con Redis
			console.log("urlRedis = " + urlRedis + "\n");
			this.urlRedis = urlRedis;
		}
		async openConnection() {

			try {
				this.conexion = redis.createClient({ url: this.urlRedis });
				await this.conexion.connect();
				return true;
			} catch (err) {
				//Indicamos que se ha producido un error en el servidor
				console.log("LogicaBD.js: Error connecting to redis server ");
				console.log(err);
				process.exit(1)
			}

		}
		//Comprobar que la clave a insertar no exista para un usuario dado (si no la encuentra devuelve undefined)
		async keyExist(owner, key) {

			try {
				//Recuperar las claves de todos los objetos de un propietario. Devuelve un array de arrays [[clave1],[clave2]]
				var arr = await this.conexion.json.objKeys(owner, '$[:]')
				//Verificar que la clave del nuevo objeto no existe previamente
				let key_exists = arr.find((item) => {
					if (item[0] == key) {
						return true;
					}
				});				
				return key_exists
			} catch (error) {
				console.log(error);
				return { value: false, msg: "Error getting owner keys" }
			}

		}
		// Ejemplo de uso de RedisJSON: https://www.npmjs.com/package/@node-redis/json
		// NOTA: Es necesario lanzar un servidor de REDIS con el modulo RedisJSON instalado
		// docker run -d -p 6379:6379 redislabs/redismod:latest
		async insertObject(owner, key, object) {

			let result
			let json_value

			try {
				//Si el propietario existe se adjunta el objeto JSON al existente, si no, se agrega un nuevo objeto
				if (await this.conexion.exists(owner)) {
					//Si la clave se ha encontrado (diferente de undefined) 									
					if (!await this.keyExist(owner, key) ) {
						json_value = { [key]: object };
						result = await this.conexion.json.ARRAPPEND(owner, '$', json_value);
					} else {
						return { value: false, msg: "Duplicate key" }
					}
				} else {
					json_value = [{ [key]: object }];
					result = await this.conexion.json.set(owner, '$', json_value);
				}
				//Comprobar que se produzca la insercion
				if (result == 'OK' || result != null) {
					return { value: true };
				}else{
					return { value: false, msg: "Object insertion failed" }
				}
			} catch (error) {
				console.log(error);
				return { value: false, msg: error }
			}
			//const value = await this.conexion.json.get('rafa',{ path: '$[:].clave1.objetos[2]'});
		}

		async getObject(owner, key) {
			
			try {
				//Comprobar si el propietario existe
				if (await this.conexion.exists(owner)) {
					//Si la clave se ha encontrado (diferente de undefined) devolvemos el objeto									
					if (await this.keyExist(owner, key) ) {
						const object = await this.conexion.json.get(owner,{ path: '$[:].'+key});
						return { value: object }
					} else {
						return { value: null, msg: "Key does not exist" }
					}
				} else {
					return { value: null, msg: "Owner does not exist" }
				}				
			} catch (error) {
				console.log(error);
				return { value: null, msg: error }
			}			
		}

		async getObjects(owner) {
			
			try {
				//Comprobar si el propietario existe
				if (await this.conexion.exists(owner)) {										
					const object = await this.conexion.json.get(owner,{ path: '$[:].*'});
					return { value: object }				
				} else {
					return { value: null, msg: "Owner does not exist" }
				}				
			} catch (error) {
				console.log(error);
				return { value: null, msg: error }
			}			
		}

		async closeConnection() {
			await this.conexion.quit();
		} // ()
	}
