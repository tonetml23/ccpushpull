const { Console } = require("console");

console.log("Corriendo bucle infinito")

//Funcion para parar la ejecucion
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function main() {
    while (true) {
        await sleep(4000)
    }
}

main()
