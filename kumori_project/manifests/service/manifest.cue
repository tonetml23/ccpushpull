package pw_service

import (
  k "kumori.systems/kumori/kmv"
  f "kumori.systems/examples/pw_service/components/frontend"
  w "kumori.systems/examples/pw_service/components/worker"
  n "kumori.systems/examples/pw_service/components/nats"
  d "kumori.systems/examples/pw_service/components/ddbb"
  r "kumori.systems/examples/pw_service/components/ddbbreplica"
)

#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "pw_service"
    version: [0,0,1]
  }

  description: {

    //
    // Kumori Component roles and configuration
    //

    // Configuration (parameters and resources) to be provided to the Kumori
    // Service Application.
    config: {
      parameter: {
        language: string
      }
      resource: {}
    }

    // List of Kumori Components of the Kumori Service Application.
    role: {
      frontend: k.#Role
      frontend: artifact: f.#Manifest
      
      worker: k.#Role
      worker: artifact: w.#Manifest
      
      nats: k.#Role
      nats: artifact: n.#Manifest
      
      ddbb: k.#Role
      ddbb: artifact: d.#Manifest
      
      ddbbreplica: k.#Role
      ddbbreplica: artifact: r.#Manifest
    }

    // Configuration spread:
    // Using the configuration service parameters, spread it into each role
    // parameters
    role: {
      frontend: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      worker: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      nats: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      ddbb: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
      ddbbreplica: {
        cfg: {
          parameter: {}
          resource: {}
        }
      }
    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    // Connectivity of a service application: the set of channels it exposes.
    srv: {
      server: {
        pushpull: { protocol: "http", port: 80 }
      }
    }

    // Connectors, providing specific patterns of communication among channels.
    connector: {
        frontendconnector: { kind: "lb" }
        ddbbfrontend: { kind: "lb" }
        natsfrontend: { kind: "full" }
        natsworker: { kind: "full" }
        ddbbworker: { kind: "full" }
        ddbbreplicamaster: { kind: "full" }
    }

    // Links specify the topology graph.
    link: {
        // Outside -> FrontEnd (LB connector)
        self: pushpull: to: "frontendconnector"
        frontendconnector: to: frontend: "restapi"
        
        frontend: conexionnats: to: "natsfrontend"
        natsfrontend: to: nats: "servernats"
        
        worker: conexionnats: to: "natsworker"
        natsworker: to: nats: "servernats"
        
        frontend: conexionddbb: to: "ddbbfrontend"
        ddbbfrontend: to: ddbbreplica: "serverreplica"
        
        worker: conexionddbb: to: "ddbbworker"
        ddbbworker: to: ddbb: "serverddbb"
        
        ddbbreplica: conexionddbb: to: "ddbbreplicamaster"
        ddbbreplicamaster: to: ddbb: "serverddbb"
    }
  }
}

