package pw_deployment

import (
  k "kumori.systems/kumori/kmv"
  s "kumori.systems/examples/pw_service/service:pw_service"
)

#Manifest: k.#DeploymentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "pw_cfg"
    version: [0,0,1]
  }

  description: {

    service: s.#Manifest

    configuration: {
      // Assign the values to the service configuration parameters
      parameter: {
        language: "en"
      }
      resource: {}
    }

    hsize: {
      frontend: {
        $_instances: 1
      }
      worker: {
        $_instances: 2
      }
      nats: {
        $_instances: 1
      }
      ddbb: {
        $_instances: 1
      }
      ddbbreplica: {
        $_instances: 2
      }
    }

  }
}

// Exposed to be used by kumorictl tool (mandatory)
deployment: (k.#DoDeploy & {_params:manifest: #Manifest}).deployment
