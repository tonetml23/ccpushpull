package ddbbreplica

import (
  k "kumori.systems/kumori/kmv"
  "strconv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "ddbbreplica"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        serverreplica: { protocol: "http", port: 6379 }
      }
      client: {
        conexionddbb: { protocol: "http" }
      }
    }

    config: {
      // Frontend role configuration parameters
      parameter: {
        appconfig: {
        }
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      ddbbreplica: {
        name: "ddbbreplica"

        image: {
          hub: {
            name: "registry.gitlab.com"
            secret: ""
          }
          tag: "tonetml23/ccpushpull/ddbb-replica-final"
        }
      }
    }
  }
}
