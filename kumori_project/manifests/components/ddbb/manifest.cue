package ddbb

import (
  k "kumori.systems/kumori/kmv"
  "strconv"
)

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "ddbb"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        serverddbb: { protocol: "http", port: 6379 }
      }
    }

    config: {
      // Frontend role configuration parameters
      parameter: {
        appconfig: {
          urlnats: "0.conexionnats:4222"
          urlredis: "0.conexionredis:6379"
        }
      }
      resource: {}
    }

    size: {
      $_memory: "100Mi"
      $_cpu: "100m"
      $_bandwidth: "10M"
    }

    code: {

      ddbb: {
        name: "ddbb"

        image: {
          hub: {
            name: "registry.gitlab.com"
            secret: ""
          }
          tag: "tonetml23/ccpushpull/redis-final"
        }

        mapping: {
          // Filesystem mapping: map the configuration into the JSON file
          // expected by the component
          filesystem: [
            {
              path: "/config/config.json"
              data: config.parameter.appconfig
              format: "json"
            }
          ]
          env: {
            CONFIG_FILE: value: "/config/config.json"
            HTTP_SERVER_PORT_ENV: value: strconv.FormatUint(srv.server.serverddbb.port, 10)
          }
        }
      }

    }
  }
}
