#!/bin/bash

INBOUNDNAME="rafa.tonet/inbound"
DEPLOYNAME="rafa.tonet/pushpulldep"
SERVICEURL="pushpullrafatonet.vera.kumori.cloud"
#SERVICEURL="volumes-demo.test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard.vera.kumori.cloud"
#CLUSTERCERT="cluster.core/wildcard.test.kumori.cloud"
TESTPATH="../test/"

case $1 in

'deploy-inbound')
  kumorictl register inbound $INBOUNDNAME \
    --domain $SERVICEURL \
    --cert $CLUSTERCERT
  ;;

'deploy-service')
  kumorictl register deployment $DEPLOYNAME \
    --deployment ./manifests/deployment \
    --comment "Volumes example service"
  ;;

'link')
  kumorictl link $DEPLOYNAME:pushpull $INBOUNDNAME
  ;;
  
'describe')
  kumorictl describe deployment $DEPLOYNAME
  ;;

# Test the volumes service
'test')
  echo "This test is located in the root folder of this project."
  echo ""
  (cd $TESTPATH && ./test.sh "https://${SERVICEURL}")
  ;;

'undeploy-service')
  kumorictl unregister deployment $DEPLOYNAME --force
  ;;

'undeploy-inbound')
  kumorictl unregister inbound $INBOUNDNAME
  ;;
  
'--help')
  echo "USAGE: $0 [option]"
  echo "    deploy-inbound"
  echo "    deploy-service"
  echo "    link"
  echo "    undeploy-service"
  echo "    undeploy-inbound"
  echo "    test"

esac
