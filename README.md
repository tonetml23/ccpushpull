# PROYECTO PUSH-PULL DESPLEGADO EN KUMORI
Este repositorio contiene el código y configuración del proyecto Push-pull.

## Uso de la aplicación
La aplicación permite insertar objetos JSON identificados por una clave y un usuario. Para ello, en la API REST desarrollada se ofrece un endpoint con diferentes rutas de tipo POST para usar los servicios. Los parámetros se deben incluir en el cuerpo de la petición, que debe estar en formato JSON.

### /insertar
Inserta un objeto JSON en el sistema. Los parámetros que debe incluir el cuerpo son usuario, clave, y valor. Cada objeto de un usuario debe tener una clave única, por lo que si se especifica una clave que ya existe para el mismo usuario no se inserta el objeto. A continuación se muestra un cuerpo de ejemplo para esta petición.

    {
        "usuario": "Pepito",
        "clave": "tareas1",
        "valor":
        {
                "dia": "martes",
                "tarea": "desayunar",
                "objetos": ["cafetera", "tostada", "leche", "cafe"]
        }
    }


### /buscarPorClave
Busca un objeto del usuario indicado, que tenga la clave exacta indicada. Los parámetros que debe incluir el cuerpo de la petición son **usuario** y **valor-busqueda**. A continuación se muestra un cuerpo de ejemplo para esta petición.

    {
        "usuario": "Pepito",
        "valor-busqueda": "tareas1"
    }


### /buscarTodos
Busca todos los objetos de un usuario. Los parámetros a indicar son el **usuario**. Un ejemplo del cuerpo de esta petición se muestra a continuación.

    {
        "usuario": "Pepito"
    }


### /buscarPorValor
Busca aquellos objetos del usuario indicado que contienen la cadena indicada. Esta petición debe incluir los parámetros **usuario** y **valor-busqueda**. A continuación se muestra un ejemplo del cuerpo de esta petición.

    {
        "usuario": "Pepito",
        "valor-busqueda": "leche"
    }

    
## Despligue de la aplicación
Para desplegar la aplicación se ha proporcionado un script con utilidades para este fin. Este script se encuentra en la carpeta **kumori_project** y se llama **utils.sh**. Para obtener una lista de los comandos disponibles se debe ejecutar el comando **./utils.sh –help**. A continuación se ofrece la lista de comandos para desplegar la aplicación. Estos comandos se deben ejecutar en la carpeta donde se encuentra este script.

    ./utils.sh deploy-inbound
    ./utils.sh deploy-service
    ./utils.sh link

En el repositorio, en la carpeta **Test**, se encuentra el script **test.sh** para probar los diferentes servicios, así como cuerpos de ejemplo para todas las peticiones.
